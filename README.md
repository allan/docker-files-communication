Bind mount
==========

Basically to bind a directory to a container you need to pass the absolute path as a volume parameter at the creation of the container :
```sh
docker run -it --mount type=bind,source="dir-in-host/dockertemp",target="dir-in-container/hosttemp"
#or
docker run -it -v "dir-in-host/dockertemp":"dir-in-container/hosttemp"[:"(ro|z|Z)"] alpine
```
- `--mount` options
    - `souce` or `src`
    - `destination` or `dest` or `target`
- `-v` options
    - `"in-host":"in-container"[:"option"]`
    - `ro` : read only
    - `z` : shared by many containers
    - `Z` : exclusive to container

- when using `-v` without a proper vol, it tries to bind mount.
- As of volumes, bind mounts could only be informed at the creation of the container.
- As of volumes, the option --mount is preferred if available.
- If you don't pass the absolute path a volume will be created and then you'd need to check the section "Mounts" with the command _docker inspect container-id_ to find the directory created for the volume, and also the volume should be deleted with _docker volume rm volume-name_.
- Since the file used in the host is a common directory, the deletion of the container won't affect the host files.
- Not recommended by docker, volumes are recommended instead because of the auto configuration (name-based and docker-space-usage) against the manual configuration (specific-path-based and user-defined-space-usage) from bind. In other words, docker aims to help newbies and noobs, but bind mounts could be useful if used properly.

Volume mount
============

[**C** RUD] Create the volume
```sh
docker volume create foobarvol
```
[C **R** UD] Show information about the volume, such as its mounting area
```sh
docker volume inspect foobarvol
```

[CR **U** D] Use a volume in a container
```sh
docker run -it --name foobar --mount source=foobarvol,target="dir-in-container/host"[:"(ro|z|Z)"] alpine sh
#or
docker run -it --name foobar -v foobarvol:"dir-in-container/host"[:"(ro|z|Z)"] alpine sh
```
- It requires a volume created with the name
- Prefer --mount over --volume option because it is easier to use (if available).
- Diff --volume and --mount : --volume is auto and --mount is explicit (verbose config)
- The order of parameters matters only with -v option

[HOST] Check file created in a volume

Inside the container, do:
```sh
touch "/host/$(date)"
```
Then on host check the mountpoint of the volume. _You may need to be root to check the directory._
- The container could be killed or deleted
- There should be a file containing the name given.

[CR **U** D] To use volumes from another container

It will basically set the volumes used in a container to the new one
```sh
docker run -it --volumes-from container-id alpine
```

[**C** R **U** D] To include volumes in already existing containers

Unfortunately _docker exec_ does not support it yet,so, we need to create a new image of the container and then create a new container from it :
```sh
docker commit base-container-id target-image-name
docker run -it -v foobarvol:"dir-in-container/host"[:"(ro|z|Z)"] target-image-name #only -v was available for me
```
And then, all files of the base-container-id should be there alongside with the mounted volumes.

[CRU **D**] Removing a volume is straightforward

_WARNING : all files inside the volume will be lost_

Just type
```sh
docker volume rm foobarvol
```

File copy
=========

File copying with docker between host and container is pretty straightforward and could be resumed in :

- From host to container

```sh
docker cp foo.txt container:/bar.txt
```

- From container to host
 
```sh
docker cp container:/foo.txt bar.txt
```